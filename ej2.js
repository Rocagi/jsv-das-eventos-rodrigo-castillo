let vector = [1, true, 'hola', 4.5, false, 'mundo'];

// a. Imprimir en la consola cada valor usando "for"
console.log("Imprimir en la consola cada valor usando 'for':");
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b. Idem al anterior usando "forEach"
console.log("Imprimir en la consola cada valor usando 'forEach':");
vector.forEach(function(valor) {
  console.log(valor);
});

// c. Idem al anterior usando "map"
console.log("Imprimir en la consola cada valor usando 'map':");
vector.map(function(valor) {
  console.log(valor);
});

// d. Idem al anterior usando "while"
console.log("Imprimir en la consola cada valor usando 'while':");
let i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

// e. Idem al anterior usando "for..of"
console.log("Imprimir en la consola cada valor usando 'for..of':");
for (let valor of vector) {
  console.log(valor);
}
