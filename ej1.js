let vector = [1, true, 'hola', 4.5, false, 'mundo'];
console.log(vector); // a. Imprimir en la consola el vector

console.log(vector[0]); // b. Imprimir en la consola el primer elemento del vector usando su índice
console.log(vector[5]); // b. Imprimir en la consola el último elemento del vector usando su índice

vector[2] = 'adiós'; // c. Modificar el valor del tercer elemento
console.log(vector[2]); // c. Imprimir en la consola el valor del tercer elemento

console.log(vector.length); // d. Imprimir en la consola la longitud del vector

vector.push(7); // e. Agregar un elemento al vector usando "push"
console.log(vector); // e. Imprimir en la consola el vector actualizado

let ultimoElemento = vector.pop(); // f. Eliminar el último elemento del vector usando "pop" e imprimirlo
console.log(ultimoElemento);
console.log(vector);

vector.splice(3, 0, 'nuevo'); // g. Agregar un elemento en la mitad del vector usando "splice"
console.log(vector); // g. Imprimir en la consola el vector actualizado

let primerElemento = vector.shift(); // h. Eliminar el primer elemento usando "shift"
console.log(primerElemento);
console.log(vector);

vector.unshift(primerElemento); // i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
console.log(vector);
